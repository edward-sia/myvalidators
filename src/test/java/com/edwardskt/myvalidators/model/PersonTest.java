/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edwardskt.myvalidators.model;

import com.edwardskt.myvalidators.groups.DraftCheck;
import com.edwardskt.myvalidators.groups.SubmitChecks;
import com.edwardskt.myvalidators.groups.UploadChecks;
import java.util.Iterator;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.equalTo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author edwardsia
 */
public class PersonTest {
  
  private Validator validator;
  public PersonTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
    validator = Validation.buildDefaultValidatorFactory().getValidator();
  }
  
  @After
  public void tearDown() {
  }

  @Test
  public void testSequenceCheck() {

    Person person = new Person();
    Set<ConstraintViolation<Person>> violations = validator.validate(person);
    
    // Fail at minimum violations in first DraftCheck
    assertEquals(2, violations.size());
  }
  
  @Test
  public void testNextSequenceTriggered_UploadCheck() {

    Person person = new Person("First", "Last");
    Set<ConstraintViolation<Person>> violations = validator.validate(person);
    Iterator<ConstraintViolation<Person>> iterator = violations.iterator();

    assertEquals("role", iterator.next().getPropertyPath().toString());
    // As Draft is satisfied, the next check is triggered, which is UploadCheck
    assertEquals(1, violations.size());
  }
  
  @Test
  public void testNextSequenceTriggered_UploadFieldsCheck() {

    Person person = new Person("First", "Last");
    person.setRole(new Role());
    Set<ConstraintViolation<Person>> violations = validator.validate(person);

    Iterator<ConstraintViolation<Person>> iterator = violations.iterator();
    
    iterator.forEachRemaining(action -> {
      assertThat(action.getPropertyPath().toString(), anyOf(equalTo("role.responsibility"), equalTo("role.responsibility2")));
    });
//    
//    assertEquals("role.responsibility", iterator.next().getPropertyPath().toString());
//    assertEquals("role.level", iterator.next().getPropertyPath().toString());

    // As Upload is satisfied, the next check is triggered, which is SubmitCheck
    assertEquals(2, violations.size());
  }
  
  @Test
  public void testNextSequenceTriggered_SubmitCheck() {

    Person person = new Person("First", "Last");
    person.setRole(new Role("asd", "1"));
    Set<ConstraintViolation<Person>> violations = validator.validate(person);

    Iterator<ConstraintViolation<Person>> iterator = violations.iterator();
    
    assertEquals("title", iterator.next().getPropertyPath().toString());
    
    // As Upload is satisfied, the next check is triggered, which is SubmitCheck
    assertEquals(1, violations.size());
  }
  
  @Test
  public void testSubmitCheckOnly() {

    Person person = new Person();
    Set<ConstraintViolation<Person>> violations = validator.validate(person, SubmitChecks.class);

    // Only SubmitCheck fields will be violated
    assertEquals(1, violations.size());
    
  }
  
  @Test
  public void testAllCheck() {

    Person person = new Person();
    Set<ConstraintViolation<Person>> violations = validator.validate(person, DraftCheck.class, UploadChecks.class, SubmitChecks.class);
    
    // All violations specified in the groups will be checked
    assertEquals(4, violations.size());
  }
  
}
