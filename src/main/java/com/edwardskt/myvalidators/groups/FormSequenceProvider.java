/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edwardskt.myvalidators.groups;

import com.edwardskt.myvalidators.model.Person;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.validator.spi.group.DefaultGroupSequenceProvider;
/**
 *
 * @author edwardsia
 */
public class FormSequenceProvider implements DefaultGroupSequenceProvider<Person> {
  @Override
  public List<Class<?>> getValidationGroups(Person person) {
    List<Class<?>> defaultGroupSequence = new ArrayList<>();
    defaultGroupSequence.add(Person.class);
    defaultGroupSequence.add(DraftCheck.class);
    defaultGroupSequence.add(UploadChecks.class);
    defaultGroupSequence.add(SubmitChecks.class);
    return defaultGroupSequence;
  }
}
