/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edwardskt.myvalidators.model;

import com.edwardskt.myvalidators.groups.UploadChecks;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author edwardsia
 */
public class Role {
  
  @NotNull(groups = UploadChecks.class)
  @Size(max = 30)
  private String responsibility;
  
  @NotNull(groups = UploadChecks.class)
  private String responsibility2;

  public Role() {}
  
  public Role(String r, String r2) {
    this.responsibility = r;
    this.responsibility2 = r2;
  }
  
}
