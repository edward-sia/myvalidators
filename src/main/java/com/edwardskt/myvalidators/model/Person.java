/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edwardskt.myvalidators.model;

import com.edwardskt.myvalidators.groups.DraftCheck;
import com.edwardskt.myvalidators.groups.FormSequenceProvider;
import com.edwardskt.myvalidators.groups.SubmitChecks;
import com.edwardskt.myvalidators.groups.UploadChecks;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.groups.ConvertGroup;
import javax.validation.groups.Default;
import org.hibernate.validator.group.GroupSequenceProvider;

/**
 *
 * @author edwardsia
 */
@GroupSequenceProvider(FormSequenceProvider.class)
public class Person {

  @NotNull(groups = SubmitChecks.class)
  private String title;
  
  @NotNull(groups = DraftCheck.class)
  private String firstName;
  
  @NotNull(groups = DraftCheck.class)
  private String lastName;
  
  @NotNull(groups = UploadChecks.class)
  @Valid
  private Role role;
  
  Person() {}
  
  Person(String firstName, String lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }
  
  public void setRole(Role role) {
    this.role = role;
  }
}
