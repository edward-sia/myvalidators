Purpose:
To create a POC of validations on a group of fields in a sequencing order using JSR380.

```
Field A - Group1
Field B - Group1

Field C - Group2

Field D - Group3

Validation sequencing - Group1 -> Group2 -> Group3
```

To understand the e2e architecture to get a look and feel of scalability, maintainability, and user experience.

Source:

https://beanvalidation.org/2.0/spec/#constraintdeclarationvalidationprocess-groupsequence
